﻿using UnityEngine;

public class PythagorasTree : MonoBehaviour {

    public Mesh mesh;
    public Material material;

    [Tooltip("Number of recursions")]
    [Range(1, 14)]
    public int depth = 4;
    private int depthPrevious; // Remember current depth to recognize changes

    [Tooltip("Upper edge of the triangle")]
    public Vector2 trianglePoint = new Vector3(0.02f, 0.35f);
    private Vector2 trianglePointWind;     // Triangle point under wind influence
    private Vector2 trianglePointPrevious; // Remember current triangle to recognize changes

    private Vector3 direction = Vector3.up;

    [Tooltip("Enable Wind")]
    public bool useWind = true;

    [Tooltip("Horizontal Wind Power")]
    [Range(-2, 2)]
    public float windStrengthH = 0.1f;
    [Tooltip("Vertical Wind Power")]
    [Range(-2, 2)]
    public float windStrengthV = 0.03f;

    [Tooltip("Horizontal Wind Speed")]
    [Range(-2, 2)]
    public float windSpeedH = 0.8f;
    [Tooltip("Vertical Wind Speed")]
    [Range(-2, 2)]
    public float windSpeedV = 0.4f;

    private float windCounter = 0; // Messure time for dynamic changes

    // Remember reference for later updates
    private PythagorasTree leftChild = null;
    private PythagorasTree rightChild = null;
    
    private void Start() {
        depthPrevious = depth;
        direction = Quaternion.Euler(transform.eulerAngles) * Vector3.up;
        gameObject.AddComponent<MeshFilter>().mesh = mesh;
        gameObject.AddComponent<MeshRenderer>().material = material;
        trianglePointWind = trianglePoint;

        if (depth > 1) {
            CreateChilds();
        }
    }

    // Check for changed values
    private void Update() {
        if (depthPrevious != depth) {
            depthPrevious = depth;

            // Depth increased: Add new fractals
            if (depth > 1 && leftChild == null) {
                CreateChilds();
            }
            // Depth decreased: Remove fractals
            if (depth <= 1 && leftChild != null) {
                Destroy(leftChild.gameObject);
                Destroy(rightChild.gameObject);
                leftChild = null;
                rightChild = null;
            }
            UpdateCalcutaltions();
        } else if (trianglePointPrevious != trianglePoint) {
            trianglePointPrevious = trianglePoint;

            UpdateCalcutaltions();
        } else if (useWind) {
            UpdateCalcutaltions();
        }
    }

    // Create new fractals and start the next recusions
    private void CreateChilds() {
        leftChild = new GameObject("FractalLeft").AddComponent<PythagorasTree>();
        rightChild = new GameObject("FractalRight").AddComponent<PythagorasTree>();
        Initialize();
    }

    private void Initialize() {
        leftChild.Initialize(this, false);
        rightChild.Initialize(this, true);
    }
    private void Initialize(PythagorasTree parent, bool right) {
        // Use the same mesh as the parent
        mesh = parent.mesh;

        // Use new material instances to have different colors
        material = Material.Instantiate(parent.material);

        // Create hierarchy
        transform.parent = parent.transform;

        // Also use wind? But add some time...
        useWind = parent.useWind;
        windCounter = parent.windCounter + 0.2f;

        UpdateCalculations(parent, right);
    }

    private void UpdateCalcutaltions() {
        // Colors
        float hue = 0.6f - ((float)(depth) * 0.05f);
        material.color = Color.HSVToRGB(hue, 1, 1);
        material.SetColor("_EmissionColor", material.color);

        if (leftChild == null) {
            return;
        }
        leftChild.UpdateCalculations(this, false);
        rightChild.UpdateCalculations(this, true);
    }
    private void UpdateCalculations(PythagorasTree parent, bool right) {

        depth = parent.depth - 1;
        trianglePointPrevious = parent.trianglePointPrevious;
        float parentSideHalfH = parent.transform.lossyScale.x / 2;
        float parentSideHalfV = parent.transform.lossyScale.y / 2;

        if (useWind) {
            windCounter += Time.deltaTime;
            trianglePointWind = new Vector2(parent.trianglePoint.x + Mathf.Sin(windCounter * windSpeedH) * windStrengthH,
                                            parent.trianglePoint.y + Mathf.Sin(windCounter * windSpeedV) * windStrengthV);
        } else {
            trianglePointWind = trianglePoint;
        }
        trianglePoint = parent.trianglePointWind;

        float angle;
        Vector3 trianglePoint3 = trianglePointWind;
        if (right) {
            angle = AngleSigned(trianglePoint3 - new Vector3(0.5f, 0, 0), new Vector3(-1f, 0, 0), Vector3.back);
        } else {
            angle = AngleSigned(trianglePoint3 + new Vector3(0.5f, 0, 0), new Vector3(1f, 0, 0), Vector3.back);
        }

        // Edge of parent
        Vector3 p1 = parent.transform.position + parent.direction * parentSideHalfV + Quaternion.Euler(0, 0, right ? -90 : 90) * parent.direction * parentSideHalfH;

        // Top triangle point
        Vector3 p2 = parent.transform.position + parent.direction * parentSideHalfV + Quaternion.Euler(0, 0, AngleSigned(parent.direction, Vector3.up, Vector3.back)) * trianglePointWind * parent.transform.lossyScale.x;

        // Middle between p1 and p2
        Vector3 pm = p1 + (p2 - p1) / 2;
        
        direction = Quaternion.Euler(0, 0, angle) * parent.direction;

        float distanceP1P2 = Vector3.Distance(p1, p2);
        transform.localScale = new Vector3(distanceP1P2 / parent.transform.lossyScale.x, distanceP1P2 / parent.transform.lossyScale.y, 1);

        transform.position = pm + direction * transform.localScale.x * parent.transform.lossyScale.x / 2;
        transform.localEulerAngles = Quaternion.Euler(0, 0, angle).eulerAngles;
    }

    /// <summary>
    /// Determine the signed angle between two vectors, with normal 'n'
    /// as the rotation axis.
    /// </summary>
    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n) {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }
}
