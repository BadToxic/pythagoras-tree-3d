# Pythagoras Tree 3D


A simple Pythagoras Tree made in Unity3D.

The Pythagoras tree is a plane fractal constructed from squares.
Invented by the Dutch mathematics teacher
Albert E. Bosman in 1942, it is named
after the ancient Greek mathematician Pythagoras
because each triple of touching squares encloses a right triangle,
in a configuration traditionally used to depict the Pythagorean theorem.
If the largest square has a size of L × L, the entire
Pythagoras tree fits snugly inside a box of size 6L × 4L.
The finer details of the tree resemble the Lévy C curve.
[Wikipedia]

by BadToxic (http://badtoxic.de)

Need support? Join my discord server: https://discord.gg/8QMCm2d

Support me with donations, likes and follows:
[Patreon](https://www.patreon.com/badtoxic)
[Instagram](https://www.instagram.com/xybadtoxic)
[Twitter](https://twitter.com/BadToxic)
[YouTube](https://www.youtube.com/user/BadToxic)
[Twitch](https://www.twitch.tv/xybadtoxic)